%define DICTIONARY 0 ; link to the first element ( last added )
%macro colon 2
	%ifid %2 ; check second arg type
		%2:
		dq DICTIONARY
	%else
		%error "error in second argument: must be a label"
	%endif
	%ifstr %1 ; check first arg type
		db %1, 0
	%else
		%error "error in first argument: must be a string"
	%endif
	%define DICTIONARY %2 ; update link
%endmacro