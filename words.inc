%include "colon.inc"

colon "word6", first_third_word
db "3+third word explanation", 0

colon "word5", first_second_word
db "2+second word explanation", 0 

colon "word4", first_first_word
db "1+first word explanation", 0

colon "word3", third_word
db "third word explanation", 0

colon "word2", second_word
db "second word explanation", 0 

colon "word1", first_word
db "first word explanation", 0 