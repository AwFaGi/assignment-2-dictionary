section .text
global exit
global string_length
global print_string
global print_error
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

 
; Принимает код возврата и завершает текущий процесс
exit: 
    ; suppose that return code is in 'rdi'
    mov rax, 60 ; exit-code
    syscall

string_length:
    ; suppose that string pointer is in 'rdi'
    xor rax, rax ; clear rax
    .loop:
        cmp byte [rdi+rax], 0 ; check if current symbol is null
        je .end ; finish if so
        inc rax ; otherwise increase string length counter
        jmp .loop ; and continue counting
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    ; suppose that string pointer is in 'rdi'
    push rdi ; save must-to-survive register before calling -->
    call string_length ; string size is in 'rax'
    pop rsi ; and restore string pointer in rsi             <--
    
    mov rdx, rax ; move string_size to 'rdx'
    mov rax, 1 ; sys_write
    mov rdi, 1 ; to stdout
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stderr
print_error:
    ; suppose that string pointer is in 'rdi'
    push rdi ; save must-to-survive register before calling -->
    call string_length ; string size is in 'rax'
    pop rsi ; and restore string pointer in rsi             <--
    
    mov rdx, rax ; move string_size to 'rdx'
    mov rax, 1 ; sys_write
    mov rdi, 2 ; to stderr
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    ; suppose that char code is in 'rdi'
    push rdi ; store char
    
    mov rax, 1 ; sys_write
    mov rdi, 1 ; to stdout
    mov rsi, rsp ; char address
    mov rdx, 1 ; char size
    syscall

    pop rdi ; return stack in before-call state
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    ; suppose that uint is in rdi
    xor rax, rax ; clear rax
    xor rdx, rdx ; clear rdx
    mov rsi, rsp ; save stack pointer
    mov rcx, 10 ; divisor
    mov rax, rdi ; move uint to acc
    push 0 ; null-terminator
    .loop:
        xor rdx, rdx ; clear rdx (rest)
        div rcx ; make division
        add dl, 0x30 ; get ascii-char
        dec rsp ; move stack pointer up
        mov [rsp], dl ; and save digit to stack
        test rax, rax ; check if there is 0 in rax
        jnz .loop ; continue divising if not 0
    mov rdi, rsp ; prepare for print_string
    push rsi ; save must-to-survive register
    call print_string ; print uint
    pop rsi ; restore saved register
    mov rsp, rsi ; return stack in before call state
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    ; suppose that int is in rdi
    test rdi, rdi ; check int
    jns .print ; if positive, it's like uint
    ; else
    neg rdi ; make positive
    push rdi ; save before calling other func
    mov rdi, 0x2D ; '-' char
    call print_char ; print '-'
    pop rdi ; return int

    .print:
        jmp print_uint ; print like uint
    

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    ; suppose that first pointer is in rdi
    ; suppose that second pointer is in rsi
    xor rax, rax ; clear acc
    xor rcx, rcx ; clear first temp storage
    xor rdx, rdx ; clear second temp storage
    xor r8, r8 ; clear counter

    .loop:
        mov cl, [rdi+r8] ; load symbol at counter position in 1 string
        mov dl, [rsi+r8] ; load symbol at counter position in 2 string
        cmp cl, dl ; compare them
        jne .false ; if they differ, return false
        cmp cl, 0 ; check if symbol is null-terminator
        je .true ; if so, return true
        inc r8 ; else counter++ 
        jmp .loop ; and continue

    .true:
        mov rax, 1
        ret

    .false:
        mov rax, 0
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:

    dec rsp ; create local var
    mov byte [rsp], 0x0 ; fill it with 0
    mov rax, 0 ; sys_read
    mov rdi, 0 ; from stdin
    mov rsi, rsp ; top of stack - local var
    mov rdx, 1 ; char size
    syscall
    mov al, [rsp] ; return value
    inc rsp ; unset local var
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    ; suppose that buffer pointer is in rdi
    ; suppose thar buffer size is in rsi
    xor rax, rax
    xor rdx, rdx ; counter
    dec rsi ; reserve for null-terminator
    .loop:
        push rdi ; store buf_pointer
        push rsi ; store buf_size
        push rdx ; store counter

        call read_char

        pop rdx ; restore counter
        pop rsi ; restore buf_size
        pop rdi ; restore buf_pointer

        test rax, rax ; check if end
        jz .good_end

        cmp rsi, rdx ; check if buffer kaputt
        jle .bad_end
    
        cmp rdx, 0 ; if not first symbol
        jne .wait_end ; jump next block
        
        ; check whitespaces and skip them

        cmp rax, 0x20
        je .loop

        cmp rax, 0x9
        je .loop

        cmp rax, 0xA
        je .loop

        jmp .write_char ; if not whitespace, write it to buffer

    .wait_end: ; check whitespaces
        cmp rax, 0x20
        je .good_end
        
        cmp rax, 0x9
        je .good_end

        cmp rax, 0xA
        je .good_end
        
        ; and continue (write it to buffer) if not whitespace

    .write_char:
        mov byte [rdi+rdx], al
        inc rdx ; counter++
        jmp .loop

    .good_end:
        mov byte [rdi+rdx], 0x0 ; write null-terminator
        mov rax, rdi ; return buffer pointer (rdx already contains word length)
        ret

    .bad_end:
        xor rax, rax ; return 0
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    ; suppose that string pointer is in rdi
    xor rax, rax
    xor r9, r9 ; counter
    xor rcx, rcx ; place for char
    xor rsi, rsi ; place for number
    mov r8, 10 ; base constant

    .loop:
        mov cl, [rdi+r9] ; read char
        
        cmp cl, '0' ; check left border
        jl .end 
        
        cmp cl, '9' ; check right border
        jg .end

        sub cl, 0x30 ; get digit number
        mov rax, rsi ; copy number to acc
        mul r8 ; acc*=10
        add rax, rcx ; acc+=current digit
        mov rsi, rax ; copy number to rsi
    
        inc r9 ; counter ++
        jmp .loop

    .end:
        mov rax, rsi ; number
        mov rdx, r9 ; length
        ret
    

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    ; suppose that string pointer is in rdi
    xor rax, rax
    xor rcx, rcx

    mov cl, [rdi] ; read first symbol
    cmp cl, '-' ; check for '-'
    je .neg

    jmp parse_uint ; if not '-', do the same what parse_uint does

    .neg:
        inc rdi ; parse starting from next char
        call parse_uint
        test rax, rax ; check for error
        jz .bad_end ; if so, return error
        neg rax ; else make negative
        inc rdx ; add '-' to length
        jmp .good_end ; and return

    .bad_end:
        xor rdx, rdx
        ret

    .good_end:
        ret



; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    ; suppose that string pointer is in rdi
    ; suppose that buffer pointer is in rsi
    ; suppose that buffer size is in rdx
    xor rax, rax

    push rdi ; save must-to-survive registers
    push rsi
    push rdx
    call string_length 
    pop rdx ;restore them
    pop rsi
    pop rdi    

    cmp rdx, rax ; compare string length and buffer size
    jl .bad_end ; if not enough space in buffer, return error

    xor r8, r8 ; counter
    xor rcx, rcx ; char storage
    .loop:
        mov cl, [rdi+r8] ; read char from string
        mov [rsi+r8], cl ; write it to buffer
        inc r8 ; counter++
        cmp r8, rax ; check end
        jle .loop ; equals because null-terminator must be copied too

    .bad_end:
        xor rax, rax
        ret
