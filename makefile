PROGA_NAME=bigproga
BUILD_DIRECTORY=build
DEBUG_DIRECTORY=debug
DEBUG_KEY=

.PHONY: clean build run debug

run: clean build
	echo "Running...\n"
	./$(BUILD_DIRECTORY)/$(PROGA_NAME)

build: main.o dict.o lib.o
	cd $(BUILD_DIRECTORY) && ld -o $(PROGA_NAME) $+ && cd ..

debug: DEBUG_KEY+=-g
debug: BUILD_DIRECTORY=$(DEBUG_DIRECTORY)
debug: build
	echo "Create debug version in the '"$(DEBUG_DIRECTORY)"' directory"

%.o: %.asm
	mkdir -p $(BUILD_DIRECTORY)
	nasm $(DEBUG_KEY) -f elf64 -o ./$(BUILD_DIRECTORY)/$@ $<

clean: 
	rm -rf $(BUILD_DIRECTORY)
	rm -rf $(DEBUG_DIRECTORY)