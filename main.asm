%define BUFFER_SIZE 255
%include "lib.inc"
extern find_word

section .bss
buffer: resb BUFFER_SIZE

section .rodata
error_buffer_overflow_message: db "Max string lenght is 255 bytes!", 0
error_key_not_found_message: db "Key not found!", 0
info_key_found_message: db "Key found!", 0

section .data
%include "words.inc"

section .text
global _start

_start:
	mov rdi, buffer
	mov rsi, BUFFER_SIZE
	call read_word ; read 'key' from user

	test rax, rax
	je .user_is_valenok ; check if there is overflown buffer

	inc rdx ; null-terminator
	push rdx ; save because there 'key' length is

	mov rdi, rax
	mov rsi, DICTIONARY

	call find_word ; try to find 'key' in 'dict'
	pop rdx

	test rax, rax 
	je .dictionary_is_bad ; if failed, say it to user 

	push rdx
	push rax

	mov rdi, info_key_found_message ; else, say about success
	call print_string
	call print_newline

	pop rax
	pop rdx

	add rax, 8 ; skip pointer

	add rax, rdx ; skip key string

	mov rdi, rax ; write found value
	call print_string
	call print_newline

	xor rdi, rdi ; exit
	call exit

	.user_is_valenok:
		mov rdi, error_buffer_overflow_message ; say about error
		call print_error
		call print_newline

		mov rdi, 1 ; exit with error_code 1
		call exit

	.dictionary_is_bad:
		mov rdi, error_key_not_found_message ; say about error
		call print_error
		call print_newline

		mov rdi, 2 ; exit with error code 2
		call exit