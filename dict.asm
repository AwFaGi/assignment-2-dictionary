section .text
global find_word
extern string_equals

; finds 'key' in 'dict'
; returns pointer to block where 'key' was found
; returns 0 otherwise (if 'key' was not found)

find_word:
	; suppose that 'key' string pointer is in rdi
	; suppose that 'dict' start pointer is in rsi

	xor rax, rax ; clear acc
	mov rdx, rsi ; variable for block

	.loop:
		add rsi, 8 ; there is key of current block

		push rdi
		push rdx
		call string_equals ; check if current key is the same as requested
		pop rdx
		pop rdi

		cmp rax, 1 ; if so...
		je .found ; we found it -> end

		mov rsi, [rdx] ; read next block pointer
		mov rdx, rsi ; save next block pointer

		test rdx, rdx ; check if we have next block
		je .not_found ; if no block, exit with not found error

		jmp .loop 

	.found:
		mov rax, rdx ; return block with needed key
		ret

	.not_found:
		xor rax, rax ; return 0
		ret

